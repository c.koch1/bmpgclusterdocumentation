## Explanation of qsub params

# -S /bin/bash

# Preserve environment variables
# -V

# Execute from current working directory
# -cwd

# Merge standard output and standard error into one file
# -j yes

# standard name of the job (if none is given on the command line):
# -N ms_fem_complete_run

# path for the output files
# -o /data.bmp/baltasar/q-out/ms-fem-complete


# Limit memory usage # maximales memory: anzahl kerne * 4 gb - 0.5 gb (puffer fuer alle faelle)
# -hard -l h_vmem=62G

# array range # fuer array jobs.
# -t 1-30

# num jobs at once
# -tc 20


# -q mvapich2-grannus.q


usage () {
        cat <<HELP_USAGE

    usage: source $(basename $0) num_initial_conditions
    ------------------------------
    with:
        num_initial_conditions :  how many initial conditions should be started
HELP_USAGE
}


# Check if there are no arguments
if [[ $# == 0 ]]
then
    echo "No arguments provided."
    usage

else
    # check if script was sourced. This script should be sourced because otherwise the environment variables would not be present.
    (return 0 2>/dev/null) && sourced=1 || sourced=0
    if [[ "$sourced" == "0" ]]
    then
        echo "Keep in mind that this script has to be sourced to keep the environment variables."
        echo "This means, do 'source $(basename $0) [...arguments]'" 
        exit
    fi



    # handle this scripts arguments

    ## qsub arguments
    # because array job: use 3.8 gb 
    MEMINGB=3.8
    NAME='Monodomain-ms-full'
    OUTPUT_PATH="/scratch01.local/baltasar/q-out/${NAME}"
    ARRAYSTART=1
    ARRAYSTOP=$1


    mkdir -p $OUTPUT_PATH


    qsub -S "/bin/bash" \
        -V \
        -cwd \
        -j "yes" \
        -N $NAME \
        -o $OUTPUT_PATH\
        -hard -l h_vmem="${MEMINGB}G" \
        -t $ARRAYSTART-$ARRAYSTOP\
        -tc 30\
        -q "grannus.q" \
        experiment_script.sh 
fi
