### This is the script that should run on the cluster machines
usage () {
        cat <<HELP_USAGE
        $(basename $0) 

HELP_USAGE
}


# "${SGE_TASK_ID:-1}" if variable is empty, defaults to +1 (yes, plus!)
TASK_ID="${SGE_TASK_ID:-1}"
TASK_ID_ZEROED=$(expr $TASK_ID - 1)


#echo ${ARGS[$TASKID]}
echo "Start"

# my sim id pattern with default values
SIMID="${JOB_ID:-None}-${TASK_ID_ZEROED:-no-sge-job}_${JOB_NAME:-no-sge-job}"

SIM_DIRECTORY="../cluster_solutions/${SIMID}"


cd ../ExperimentRealization/01-create_initial_conditions
python 01_create_initial_condition.py ${SIM_DIRECTORY}

wait

cd ../02-run_dynamics_and_reconstruct_phi_on_heart
python run_experiment.py ${SIM_DIRECTORY}

wait

cd ../04-dynamics_to-reg_grid 
python run_experiment.py ${SIM_DIRECTORY}

wait

cd ../03-diffuse_into_bath_and_calculate_electrode_potentials
python run_experiment.py ${SIM_DIRECTORY}


