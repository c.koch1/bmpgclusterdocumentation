#!/bin/bash

# Use python as shell
#$ -S /bin/bash

# Preserve environment variables
#$ -V

# Execute from current working directory
#$ -cwd

# Merge standard output and standard error into one file
#$ -j yes

# Standard name of the job (if none is given on the command line):
#$ -N Transient-3D

# Path for the output files
#$ -o /home/thomas/tmp/q-out/

# Limit memory usage
#$ -hard -l h_vmem=62G

# array range
#$ -t 1-1001

# parallel, Anzahl der Kerne
#$ -pe mvapich2-grannus05 16

#$ -q mvapich2-grannus05.q

python Construct_Filaments_MPI.py 40 10000 10






Das steht im Python Skript:


import os

id = int(os.getenv("SGE_TASK_ID", 0))
first = int(os.getenv("SGE_TASK_FIRST", 0))
last = int(os.getenv("SGE_TASK_LAST", 0))
print "ID", id
print("Task %d of %d tasks, starting with %d." % (id, last - first + 1, first))
print("This job was submitted from %s, it is currently running on %s"
      % (os.getenv("SGE_O_HOST"), os.getenv("HOSTNAME")))
print("NHOSTS: %s, NSLOTS: %s" % (os.getenv("NHOSTS"), os.getenv("NSLOTS")))














